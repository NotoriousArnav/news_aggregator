# Democratic Defenders
A Simple Aggregator Application to Collect news from Providers and Stores in Database for Future Processing.

## Independent Media Houses
```
Why is it becoming more Important to Follow and Support Independent Media, and what might happen to them in Future?
```

Free media is a cornerstone of a healthy democracy. When major news outlets are accused of bias and spreading propaganda, independent media becomes the last hope for saving democracy. 📰💥 It's time to rebel against partiality and undemocratic practices, and support independent voices that speak truth to power! #IndependentMedia #SaveDemocracy

## Working Model of the Project
This Repo is the API Server for Aggregating News from Several Sources and then Serve them via a Really Basic UI and an API.
Its reccomended to use Dedicated Frontends pointed towards this Server, since they will provide a really good Experience with their UI/UX as well as Provide Automation Opportunities.
